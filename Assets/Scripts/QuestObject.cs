﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestObject : MonoBehaviour
{
    public int questNumber;

    public QuestManager thisQuestManager;

    public string startText;
    public string endText;

    public bool isItemQuest;

    public string targetItem;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(isItemQuest)
        {
            if(thisQuestManager.itemCollected == targetItem)
            {
                thisQuestManager.itemCollected = null;
                EndQuest();
            }
        }
    }

    public void StartQuest()
    {
        thisQuestManager.ShowQuestText(startText);
    }

    public void EndQuest()
    {
        thisQuestManager.ShowQuestText(endText);
        thisQuestManager.questCompleted[questNumber] = true;
        gameObject.SetActive(false);
    }
}
