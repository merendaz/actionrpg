﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueManager : MonoBehaviour
{
    public GameObject dialogueBox;
    public Text dialogueText;
    public bool dialogueActive;

    public string[] dialogLines;
    public int currentLine;

    private PlayerController thisPlayer;

    // Start is called before the first frame update
    void Start()
    {
        thisPlayer = FindObjectOfType<PlayerController>();
    }

    // Update is called once per frame
    void Update()
    {
        if(dialogueActive && Input.GetKeyDown(KeyCode.Space))
        {
            currentLine++;
        }

        if(currentLine >= dialogLines.Length)
        {
            dialogueBox.SetActive(false);
            dialogueActive = false;
            currentLine = 0;
            thisPlayer.canMove = true;
        }
        dialogueText.text = dialogLines[currentLine];
    }

    //public void ShowBox(string dialogue)
    //{
    //    dialogueBox.SetActive(true);
    //    dialogueActive = true;
    //    dialogueText.text = dialogue;
    //}

    public void ShowDialogue()
    {
        dialogueBox.SetActive(true);
        dialogueActive = true;
        thisPlayer.canMove = false;
    }
}
