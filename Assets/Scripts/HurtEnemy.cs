﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HurtEnemy : MonoBehaviour
{

    public int damageToGive;
    private int currentDmage;
    public GameObject damageBurst;
    public Transform hitPoint;
    public GameObject damageNumber;
    private PlayerStats thePlayerStats;

    // Start is called before the first frame update
    void Start()
    {
        thePlayerStats = FindObjectOfType<PlayerStats>();
    }

    // Update is called once per frame
    void Update()
    {
       
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Enemy")
        {
            currentDmage = damageToGive + thePlayerStats.currentAttack;
            other.gameObject.GetComponent<EnemyHealthManager> ().HurtEnemy(currentDmage);
            Instantiate(damageBurst, hitPoint.position, hitPoint.rotation);
            var clone = (GameObject) Instantiate(damageNumber, hitPoint.position, Quaternion.Euler (Vector3.zero));
            clone.GetComponent<FloatingNumbers>().damageNumber = currentDmage;
        }
    }
}
