﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestTrigger : MonoBehaviour
{
    private QuestManager thisQuestManager;
    public int questNumber;
    public bool startQuest;
    public bool endQuest;

    // Start is called before the first frame update
    void Start()
    {
        thisQuestManager = FindObjectOfType<QuestManager>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.name == "Player")
        {
            if(!thisQuestManager.questCompleted[questNumber])
            {
                if(startQuest && !thisQuestManager.quests[questNumber].gameObject.activeSelf)
                {
                    thisQuestManager.quests[questNumber].gameObject.SetActive(true);
                    thisQuestManager.quests[questNumber].StartQuest();
                }
                if (endQuest && thisQuestManager.quests[questNumber].gameObject.activeSelf)
                {
                    thisQuestManager.quests[questNumber].EndQuest();
                }
            }
        }
    }
}
