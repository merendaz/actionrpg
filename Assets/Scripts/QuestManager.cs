﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class QuestManager : MonoBehaviour
{
    public QuestObject[] quests;
    public bool[] questCompleted;

    public DialogueManager thisDialogueManager;

    public string itemCollected;

    // Start is called before the first frame update
    void Start()
    {
        questCompleted = new bool[quests.Length];
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ShowQuestText(string questText)
    {

        thisDialogueManager.dialogLines = new string[1];
        thisDialogueManager.dialogLines[0] = questText;
        thisDialogueManager.currentLine = 0;
        thisDialogueManager.ShowDialogue();
    }
}
