﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestItem : MonoBehaviour
{
    public int questNumber;

    private QuestManager thisQuestionManager;

    public string itemName;

    // Start is called before the first frame update
    void Start()
    {
        thisQuestionManager = FindObjectOfType<QuestManager>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.name == "Player")
        {
            if(!thisQuestionManager.questCompleted[questNumber] && thisQuestionManager.quests[questNumber].gameObject.activeSelf)
            {
                thisQuestionManager.itemCollected = itemName;
                gameObject.SetActive(false);
            }
        }
    }
}
