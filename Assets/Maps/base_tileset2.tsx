<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.3.1" name="base_tileset2" tilewidth="16" tileheight="16" spacing="1" tilecount="1767" columns="57">
 <image source="../Art/roguelikeSheet_transparent.png" width="968" height="526"/>
 <terraintypes>
  <terrain name="Dirt" tile="578"/>
 </terraintypes>
 <tile id="2">
  <objectgroup draworder="index" id="3">
   <object id="2" x="14.1304" y="11.2652"/>
  </objectgroup>
 </tile>
 <tile id="3">
  <objectgroup draworder="index" id="2">
   <object id="1" x="8.33496" y="11.721"/>
  </objectgroup>
 </tile>
 <tile id="4">
  <objectgroup draworder="index" id="2">
   <object id="1" x="2.86514" y="15.9536"/>
  </objectgroup>
 </tile>
 <tile id="59">
  <objectgroup draworder="index" id="2">
   <object id="1" x="12.1769" y="8.00938"/>
  </objectgroup>
 </tile>
 <tile id="60">
  <objectgroup draworder="index" id="2">
   <object id="1" x="8.13961" y="8.33496"/>
  </objectgroup>
 </tile>
 <tile id="61">
  <objectgroup draworder="index" id="2">
   <object id="1" x="3.71166" y="10.028"/>
  </objectgroup>
 </tile>
 <tile id="116">
  <objectgroup draworder="index" id="2">
   <object id="1" x="13.0234" y="3.12561"/>
  </objectgroup>
 </tile>
 <tile id="117">
  <objectgroup draworder="index" id="2">
   <object id="1" x="7.29309" y="3.97213"/>
  </objectgroup>
 </tile>
 <tile id="118">
  <objectgroup draworder="index" id="2">
   <object id="1" x="2.93026" y="3.58143" height="0.260468"/>
  </objectgroup>
 </tile>
 <tile id="518" terrain="0,0,0,"/>
 <tile id="519" terrain="0,0,,0"/>
 <tile id="520" terrain=",,,0"/>
 <tile id="521" terrain=",,0,0"/>
 <tile id="522" terrain=",,0,"/>
 <tile id="526">
  <objectgroup draworder="index" id="2">
   <object id="1" x="7.94426" y="8.40008"/>
  </objectgroup>
 </tile>
 <tile id="575" terrain="0,,0,0"/>
 <tile id="576" terrain=",0,0,0"/>
 <tile id="577" terrain=",0,,0"/>
 <tile id="578" terrain="0,0,0,0"/>
 <tile id="579" terrain="0,,0,"/>
 <tile id="634" terrain=",0,,"/>
 <tile id="635" terrain="0,0,,"/>
 <tile id="636" terrain="0,,,"/>
 <tile id="643">
  <objectgroup draworder="index" id="2">
   <object id="1" x="8.13961" y="8.33496"/>
  </objectgroup>
 </tile>
</tileset>
