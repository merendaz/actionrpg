﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealthManager : MonoBehaviour
{
    public int maxHealth;
    public int currentHealth;

    private PlayerStats thePlayerStats;

    public int experienceToGive;

    // Start is called before the first frame update
    void Start()
    {
        currentHealth = maxHealth;

        thePlayerStats = FindObjectOfType<PlayerStats>();
    }

    // Update is called once per frame
    void Update()
    {
        if (currentHealth <= 0)
        {
            Destroy(gameObject);

            thePlayerStats.AddExperience(experienceToGive);
        }
    }

    public void HurtEnemy(int damageToGive)
    {
        currentHealth -= damageToGive;
    }

    public void SetmaxHealth()
    {
        currentHealth = maxHealth;
    }
}
