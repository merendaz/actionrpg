<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.3.1" name="red_collision" tilewidth="16" tileheight="16" spacing="1" tilecount="1" columns="1">
 <image source="../../../Resources/GamesPlusJames - Unity 2D RPG/red-collision-box.png" width="16" height="16"/>
 <tile id="0">
  <objectgroup draworder="index" id="2">
   <object id="1" x="8.36364" y="8.18182"/>
  </objectgroup>
 </tile>
</tileset>
