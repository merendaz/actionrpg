<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.3.1" name="base tileset" tilewidth="16" tileheight="16" spacing="1" tilecount="1767" columns="57">
 <image source="../Art/roguelikeSheet_transparent.png" width="968" height="526"/>
 <terraintypes>
  <terrain name="Dirt" tile="578"/>
 </terraintypes>
 <tile id="518" terrain="0,0,0,"/>
 <tile id="519" terrain="0,0,,0"/>
 <tile id="520" terrain=",,,0"/>
 <tile id="521" terrain=",,0,0"/>
 <tile id="522" terrain=",,0,"/>
 <tile id="526">
  <objectgroup draworder="index" id="2">
   <object id="2" x="7.76428" y="7.96337"/>
  </objectgroup>
 </tile>
 <tile id="575" terrain="0,,0,0"/>
 <tile id="576" terrain=",0,0,0"/>
 <tile id="577" terrain=",0,,0"/>
 <tile id="578" terrain="0,0,0,0"/>
 <tile id="579" terrain="0,,0,"/>
 <tile id="634" terrain=",0,,"/>
 <tile id="635" terrain="0,0,,"/>
 <tile id="636" terrain="0,,,"/>
 <tile id="643">
  <objectgroup draworder="index" id="2">
   <object id="2" x="7.80412" y="6.8286"/>
  </objectgroup>
 </tile>
</tileset>
